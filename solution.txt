1. List the books Authored by Marjorie Green.
 Answer:
 The Busy Executive's Database Guide
 You can combat computer stress

2. List the books Authored by Michael O'Leary.
    Answer:
    Cooking with Computers
    TC7777 ID is missing

3. Write the author/s of "The Busy Executive’s Database Guide".
    Answer:
    Marjorie Green

4. Identify the publisher of "But is it user friendly?".
    Answer:
    Algodata Infosystems

5. List the books published by Algodata Infosystems.
    Answer:
    But is it user friendly?
    The Busy Executive's Database Guide
    Cooking with Computers
    Straight talk about computers
    Secretes of Silicon Valley
    Net Etiquette
